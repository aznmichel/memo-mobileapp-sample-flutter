import 'package:flutter_test/flutter_test.dart';
import 'package:memoplace/models/memo.dart';
import 'package:mockito/mockito.dart';
import 'package:http/http.dart' as http;

import 'package:memoplace/constants/api.dart';
import 'package:memoplace/services/memo_api.dart';
import 'package:memoplace/utils/exceptions.dart';
import 'package:memoplace/utils/memoplace_responses.dart';

class MockClient extends Mock implements http.Client {}

main() {
  MemoApiService memoApiService;

  void stubGet(String url, http.Response response) {
    when(memoApiService.client.get(
      argThat(startsWith(url)),
      headers: anyNamed('headers'),
    )).thenAnswer((_) async => response);
  }

  void stubPost(String url, http.Response response) {
    when(memoApiService.client.post(
      argThat(startsWith(url)),
      headers: anyNamed('headers'),
      body: anyNamed('body'),
    )).thenAnswer((_) async => response);
  }

  void stubPut(String url, http.Response response) {
    when(memoApiService.client.put(
      argThat(startsWith(url)),
      headers: anyNamed('headers'),
      body: anyNamed('body'),
    )).thenAnswer((_) async => response);
  }

  void stubDelete(String url, http.Response response) {
    when(memoApiService.client.delete(
      argThat(startsWith(url)),
      headers: anyNamed('headers'),
    )).thenAnswer((_) async => response);
  }

  setUp(() {
    memoApiService = MemoApiService(client: MockClient());
  });

  tearDown(() {
    memoApiService = null;
  });

  const url = APIConfig.endpointAPI;

  group('getAllMemos', () {
    test('returns a list of Memos if the http call completes successfully',
        () async {
      // Use Mockito to return a successful response
      final fakeResponse =
          '{"memosList":[{"id":1,"title":"M\u00e9mo Exemple","content":"BLABLABLA by john","scope":0,"status":0,"created_at":"2020-11-23T16:19:04.000000Z","updated_at":"2020-11-23T16:19:04.000000Z"}],"message":"Ressources retrouv\u00e9es avec succ\u00e8s"}';
      stubGet('$url/memo', http.Response(fakeResponse, 200));

      final result = await memoApiService.getAllMemos();
      expect(result, isA<MemoPlaceResponse>());
    });

    test('throws an exception if the http call completes with an error', () {
      // Use Mockito to return an unsuccessful response
      final fakeResponse = '{"message":"Unauthenticated."}';
      stubGet('$url/memo', http.Response(fakeResponse, 401));

      expect(
          memoApiService.getAllMemos(), throwsA(isInstanceOf<AuthException>()));
    });
  });

  group('getMemo', () {
    const int memoID = 1;
    test('returns a specific Memo if the http call completes successfully',
        () async {
      // Use Mockito to return a successful response
      final fakeResponse =
          '{"memo":{"id":1,"title":"M\u00e9mo Exemple","content":"BLABLABLA by john","scope":0,"status":0,"created_at":"2020-11-23T16:19:04.000000Z","updated_at":"2020-11-23T16:19:04.000000Z"},"message":"Ressource retrouv\u00e9e avec succ\u00e8s"}';
      stubGet('$url/memo/$memoID', http.Response(fakeResponse, 200));

      final result = await memoApiService.getMemo(memoID);
      expect(result, isA<Memo>());
    });

    test('throws an exception if the http call completes with an error', () {
      // Use Mockito to return an unsuccessful response
      final fakeResponse = '{"message":"Unauthenticated."}';
      stubGet('$url/memo/$memoID', http.Response(fakeResponse, 401));

      expect(memoApiService.getMemo(memoID),
          throwsA(isInstanceOf<AuthException>()));
    });
  });

  group('addMemo', () {
    const fakeMemo = {
      "title": "Mémo TEST",
      "content": "BLABLABLA by john",
      "scope": "1",
      "status": "1"
    };
    test(
        'returns a specific http.Response 200 if the http call addMemo completes successfully',
        () async {
      // Use Mockito to return a successful response
      final fakeResponse =
          '{"memo":{"title":"M\u00e9mo TEST","content":"BLABLABLA by john","updated_at":"2020-11-26T10:46:16.000000Z","created_at":"2020-11-26T10:46:16.000000Z","id":13,"user":{"id":1,"name":"Michel Man","email":"aznmichel@gmail.com"}},"message":"Ressource cr\u00e9\u00e9e avec succ\u00e8s"}';
      stubPost('$url/memo', http.Response(fakeResponse, 200));

      final result =
          await memoApiService.addMemo(fakeMemo["title"], fakeMemo["content"]);
      expect(result, isA<http.Response>());
    });

    test('throws an exception if the http call addMemo completes with an error',
        () {
      // Use Mockito to return an unsuccessful response
      final fakeResponse = '{"message":"Unauthenticated."}';
      stubPost('$url/memo', http.Response(fakeResponse, 401));

      final result =
          memoApiService.addMemo(fakeMemo["title"], fakeMemo["content"]);
      expect(result, throwsA(isInstanceOf<AuthException>()));
    });
  });

  group('updateMemo', () {
    const fakeMemo = {
      "id": 1,
      "title": "Mémo TEST",
      "content": "BLABLABLA by john",
      "scope": "1",
      "status": "1"
    };
    test(
        'returns a specific http.Response 200 if the http call updateMemo completes successfully',
        () async {
      // Use Mockito to return a successful response
      final fakeResponse =
          '{"memo":{"id":1,"title":"M\u00e9mo MAJ","content":"BLABLABLA MAJ","scope":"1","status":"1","created_at":"2020-11-23T16:19:04.000000Z","updated_at":"2020-11-26T11:11:09.000000Z"},"message":"Mise \u00e0 jour effectu\u00e9e avec succ\u00e8s"}';
      stubPut('$url/memo/${fakeMemo["id"]}', http.Response(fakeResponse, 200));

      final result = await memoApiService.updateMemo(
          fakeMemo["title"], fakeMemo["content"], fakeMemo["id"]);
      expect(result, isA<http.Response>());
    });

    test(
        'throws an exception if the http call updateMemo completes with an error',
        () {
      // Use Mockito to return an unsuccessful response
      final fakeResponse = '{"message":"Unauthenticated."}';
      stubPut('$url/memo/${fakeMemo["id"]}', http.Response(fakeResponse, 401));

      final result = memoApiService.updateMemo(
          fakeMemo["title"], fakeMemo["content"], fakeMemo["id"]);
      expect(result, throwsA(isInstanceOf<AuthException>()));
    });
  });

  group('removeMemo', () {
    const fakeMemo = {
      "id": 1,
      "title": "Mémo TEST",
      "content": "BLABLABLA by john",
      "scope": "1",
      "status": "1"
    };
    test(
        'returns a specific http.Response 200 if the http call removeMemo completes successfully',
        () async {
      // Use Mockito to return a successful response
      final fakeResponse = '{"message":"Ressource supprim\u00e9e"}';
      stubDelete(
          '$url/memo/${fakeMemo["id"]}', http.Response(fakeResponse, 200));

      final result = await memoApiService.removeMemo(fakeMemo["id"]);
      expect(result, isNot(throwsException));
    });

    test(
        'throws an exception if the http call removeMemo completes with an error',
        () {
      // Use Mockito to return an unsuccessful response
      final fakeResponse = '{"message":"Unauthenticated."}';
      stubDelete(
          '$url/memo/${fakeMemo["id"]}', http.Response(fakeResponse, 401));

      final result = memoApiService.removeMemo(fakeMemo["id"]);
      expect(result, throwsA(isInstanceOf<AuthException>()));
    });
  });
}
