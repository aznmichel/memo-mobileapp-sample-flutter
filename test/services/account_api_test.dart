import 'package:flutter_test/flutter_test.dart';
import 'package:memoplace/models/user.dart';
import 'package:mockito/mockito.dart';
import 'package:http/http.dart' as http;

import 'package:memoplace/constants/api.dart';
import 'package:memoplace/services/account_api.dart';
import 'package:memoplace/utils/exceptions.dart';

class MockClient extends Mock implements http.Client {}

main() {
  AccountApiService accountApiService;

  void stubGet(String url, http.Response response) {
    when(accountApiService.client.get(
      argThat(startsWith(url)),
      headers: anyNamed('headers'),
    )).thenAnswer((_) async => response);
  }

  void stubPost(String url, http.Response response) {
    when(accountApiService.client.post(
      argThat(startsWith(url)),
      headers: anyNamed('headers'),
      body: anyNamed('body'),
    )).thenAnswer((_) async => response);
  }

  setUp(() {
    accountApiService = AccountApiService(client: MockClient());
  });

  tearDown(() {
    accountApiService = null;
  });

  const url = APIConfig.endpointAPI;

  final fakeUser = User(id: 1, name: "John Doe", email: "john@doe.com");
  final password = "toto1234";

  group('login', () {
    test(
        'returns a specific http.Response 200 if the http call login completes successfully',
        () async {
      // Use Mockito to return a successful response
      final fakeResponse =
          '{"token_type":"Bearer","expires_in":86400,"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9...","refresh_token":"6LPCjQOaqGQ-Ve5WS3gw8899MYD7q-nBeIepGVmH8G..."}';
      stubPost('$url/login', http.Response(fakeResponse, 200));

      final result = await accountApiService.login(fakeUser.email, password);
      expect(result, isA<http.Response>());
      expect(result.statusCode, 200);
    });
  });

  group('register', () {
    test(
        'returns a specific http.Response 200 if the http call register completes successfully',
        () async {
      // Use Mockito to return a successful response
      final fakeResponse =
          '{"token_type":"Bearer","expires_in":86400,"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9...","refresh_token":"6LPCjQOaqGQ-Ve5WS3gw8899MYD7q-nBeIepGVmH8G..."}';
      stubPost('$url/register', http.Response(fakeResponse, 200));

      final result = await accountApiService.register(
          fakeUser.name, fakeUser.email, password, password);
      expect(result, isA<http.Response>());
      expect(result.statusCode, 200);
    });
  });

  group('getUser', () {
    test(
        'returns a specific http.Response 200 if the http call getUser completes successfully',
        () async {
      // Use Mockito to return a successful response
      final fakeResponse = '{"id":1,"name":"John Doe","email":"john@doe.com"}';
      stubGet('$url/user', http.Response(fakeResponse, 200));

      final user = await accountApiService.getUserInfos();
      expect(user, isA<User>());
      expect(user, fakeUser);
    });

    test('throws an exception if the http call getUser completes with an error',
        () {
      // Use Mockito to return an unsuccessful response
      final fakeResponse = '{"message":"Unauthenticated."}';
      stubGet('$url/user', http.Response(fakeResponse, 401));

      expect(accountApiService.getUserInfos(),
          throwsA(isInstanceOf<AuthException>()));
    });
  });

  group('logout', () {
    test(
        'returns a specific http.Response 200 if the http call logout completes successfully',
        () async {
      // Use Mockito to return a successful response
      final fakeResponse =
          '{"message":"You have been successfully logged out!"}';
      stubPost('$url/logout', http.Response(fakeResponse, 200));

      final result = await accountApiService.logOut();
      expect(result, isNot(throwsException));
      expect(result.statusCode, 200);
    });
  });
}
