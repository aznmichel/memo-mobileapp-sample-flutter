import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:meta/meta.dart';

import 'package:memoplace/constants/api.dart';

import 'package:memoplace/models/user.dart';
import 'package:memoplace/utils/exceptions.dart';
import 'package:memoplace/utils/secure_storage.dart';

class AccountApiService {
  final http.Client client;

  AccountApiService({@required this.client});

  Future<http.Response> login(String email, String password) async {
    final url = "${APIConfig.endpointAPI}/login";

    Map<String, String> data = {
      'email': email,
      'password': password,
    };

    return await client.post(
      url,
      body: json.encode(data),
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json'
      },
    );
  }

  Future<http.Response> register(String name, String email, String password,
      String passwordConfirm) async {
    final url = "${APIConfig.endpointAPI}/register";

    Map<String, String> params = {
      'name': name,
      'email': email,
      'password': password,
      'password_confirmation': passwordConfirm,
    };

    return await client.post(
      url,
      body: jsonEncode(params),
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json'
      },
    );
  }

  Future<User> getUserInfos() async {
    final url = "${APIConfig.endpointAPI}/user";
    final token = await StorageUtil.getValue('access_token');

    final response = await client.get(
      url,
      headers: {HttpHeaders.authorizationHeader: 'Bearer $token'},
    );

    if (response.statusCode == 401) {
      throw new AuthException("401", "Unauthorized");
    }

    Map<String, dynamic> apiResponse = json.decode(response.body);
    return userFromJson(json.encode(apiResponse));
  }

  Future<http.Response> logOut() async {
    final url = "${APIConfig.endpointAPI}/logout";
    final token = await StorageUtil.getValue('access_token');

    return await client.post(
      url,
      headers: {HttpHeaders.authorizationHeader: 'Bearer $token'},
    );
  }
}
