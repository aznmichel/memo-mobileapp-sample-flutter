import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:meta/meta.dart';

import 'package:memoplace/constants/api.dart';

import 'package:memoplace/utils/exceptions.dart';
import 'package:memoplace/models/memo.dart';
import 'package:memoplace/utils/secure_storage.dart';
import 'package:memoplace/utils/memoplace_responses.dart';

class MemoApiService {
  final http.Client client;

  MemoApiService({@required this.client});

  /*
  * Validates the response code from an API call.
  * A 401 indicates that the token has expired.
  */
  void validateResponseStatus(int status) {
    if (status == 401) {
      throw new AuthException("401", "Unauthorized");
    }
  }

  // Returns a list of Memos
  Future<MemoPlaceResponse> getAllMemos() async {
    final token = await StorageUtil.getValue('access_token');

    final response = await client.get(
      "${APIConfig.endpointAPI}/memo",
      headers: {HttpHeaders.authorizationHeader: 'Bearer $token'},
    );

    validateResponseStatus(response.statusCode);

    Map<String, dynamic> apiResponse = json.decode(response.body);
    List<dynamic> data = apiResponse['memosList'];

    List<Memo> memos = memoListFromJson(json.encode(data));

    return MemoPlaceResponse(memos, "");
  }

  // Returns a specific Memo
  Future<Memo> getMemo(int id) async {
    final token = await StorageUtil.getValue('access_token');

    final response = await client.get(
      "${APIConfig.endpointAPI}/memo/$id",
      headers: {HttpHeaders.authorizationHeader: 'Bearer $token'},
    );

    validateResponseStatus(response.statusCode);

    Map<String, dynamic> apiResponse = json.decode(response.body);

    return memoFromJson(json.encode(apiResponse['memo']));
  }

  // Add a new Memo
  Future<http.Response> addMemo(String title, String content) async {
    final token = await StorageUtil.getValue('access_token');

    Map<String, String> body = {'title': title, 'content': content};

    final response = await client.post("${APIConfig.endpointAPI}/memo",
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'},
        body: body);

    validateResponseStatus(response.statusCode);

    return response;
  }

  // Update an existing Memo
  Future<http.Response> updateMemo(String title, String content, int id) async {
    final token = await StorageUtil.getValue('access_token');

    Map<String, String> body = {'title': title, 'content': content};

    final response = await client.put("${APIConfig.endpointAPI}/memo/$id",
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'},
        body: body);

    validateResponseStatus(response.statusCode);

    return response;
  }

  // Delete an existing Memo
  removeMemo(int id) async {
    final token = await StorageUtil.getValue('access_token');

    final response = await client.delete("${APIConfig.endpointAPI}/memo/$id",
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});

    validateResponseStatus(response.statusCode);
  }
}
