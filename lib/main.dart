import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// Providers
import 'package:memoplace/providers/account.dart';
import 'package:memoplace/providers/memo.dart';

// Views
import 'package:memoplace/views/login.dart';
import 'package:memoplace/views/loading.dart';
import 'package:memoplace/views/memos.dart';
import 'package:memoplace/views/register.dart';
import 'package:memoplace/views/onboarding.dart';
import 'package:memoplace/views/profile.dart';

import 'package:logger/logger.dart';

var logger = Logger();

void main() {
  runApp(MemoPlaceApp());
}

class MemoPlaceApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => AccountProvider(),
      child: MaterialApp(
        title: 'Argon Flutter - memoplace',
        theme: ThemeData(fontFamily: 'OpenSans'),
        initialRoute: '/onboarding',
        debugShowCheckedModeBanner: false,
        routes: {
          '/onboarding': (context) => Router(),
          '/login': (context) => new Login(),
          '/account': (context) => new Register(),
          '/profile': (context) => new Profile(),
          '/memos': (context) => new Memos(),
        },
      ),
    );
  }
}

class Router extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, AccountProvider account, _) {
        logger.d("NOUVEAU STATUS: " + account.status.toString());
        switch (account.status) {
          case Status.Uninitialized:
            logger.d("Status.Uninitialized...Go to Loading view");
            return Loading();
          case Status.Unauthenticated:
            logger.d("Status.Unauthenticated...Go to Onboarding view");
            return Onboarding();
          case Status.Authenticated:
            logger.d("Status.Authenticated...Go to Memos view");
            return ChangeNotifierProvider(
              create: (context) => MemoProvider(account),
              child: Memos(),
            );
          default:
            return Onboarding();
        }
      },
    );
  }
}
