import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:memoplace/providers/account.dart';
import 'package:memoplace/utils/exceptions.dart';
import 'package:memoplace/services/memo_api.dart';
import 'package:memoplace/models/memo.dart';

import 'package:memoplace/utils/memoplace_responses.dart';
import 'package:memoplace/widgets/notifications.dart';

class MemoProvider with ChangeNotifier {
  // Variables
  bool _initialized = false;
  List<Memo> _memos = List<Memo>();
  NotificationText _notification;

  // Provides access to private variables
  bool get initialized => _initialized;
  List<Memo> get memos => _memos;
  NotificationText get notification => _notification;

  // AccountProvider
  AccountProvider accountProvider;

  // API Service
  MemoApiService memoApiService;

  // Constructor
  MemoProvider(AccountProvider accountProvider) {
    this.accountProvider = accountProvider;
    this.memoApiService = MemoApiService(client: http.Client());

    init();
  }

  void init() async {
    try {
      // Get MemosList
      MemoPlaceResponse memoplaceResponse =
          await this.memoApiService.getAllMemos();
      _initialized = true;
      _memos = memoplaceResponse.memos;
      notifyListeners();

      // Get infos from currentUser
      this.accountProvider.getUser();
    } on AuthException {
      // API returned a AuthException, so user is logged out.
      await accountProvider.logOut(true);
    } catch (Exception) {
      print(Exception);
    }
  }

  void initNotification() {
    _notification = null;
    notifyListeners();
  }

  /*
  * Check the response code from an API call.
  * A 422 indicates that there are some validation errors.
  * A 200 indicates the API call was successful.
  */
  bool checkResponseStatus(http.Response response) {
    if (response.statusCode == 200)
      return true;
    else
      return false;
  }

  Future<void> addMemo(String title, String content) async {
    try {
      // Reset NotificationText
      initNotification();

      // Post the new memo
      final response = await memoApiService.addMemo(title, content);

      // Check the response status
      final check = checkResponseStatus(response);
      Map<String, dynamic> apiResponse = json.decode(response.body);

      if (check == true) {
        // Update Memos List
        Memo memoCreated = memoFromJson(json.encode(apiResponse['memo']));
        _memos.insert(_memos.length, memoCreated);
        notifyListeners();
      } else {
        _notification = NotificationText(apiResponse['error'].toString());
        notifyListeners();
      }
    } on AuthException {
      // API returned a AuthException, so user is logged out.
      await accountProvider.logOut(true);
    } catch (Exception) {
      print(Exception);
    }
  }

  Future<void> udpateMemo(String title, String content, int id) async {
    try {
      // Reset NotificationText
      initNotification();

      // Patch an existing memo
      final response = await memoApiService.updateMemo(title, content, id);

      // Check the response status
      final check = checkResponseStatus(response);
      Map<String, dynamic> apiResponse = json.decode(response.body);

      if (check == true) {
        // Update Memos List
        Memo memoUpdated = memoFromJson(json.encode(apiResponse['memo']));
        final indexCurrentMemo = _memos.indexWhere((item) => item.id == id);
        if (indexCurrentMemo != null) _memos[indexCurrentMemo] = memoUpdated;
        notifyListeners();
      } else {
        _notification = NotificationText(apiResponse['error'].toString());
        notifyListeners();
      }
    } on AuthException {
      // API returned a AuthException, so user is logged out.
      await accountProvider.logOut(true);
    } catch (Exception) {
      print(Exception);
    }
  }

  Future<void> removeMemo(int id) async {
    try {
      // Remove an existing item
      await memoApiService.removeMemo(id);

      // Update Memos List
      final indexCurrentMemo = _memos.indexWhere((item) => item.id == id);
      if (indexCurrentMemo != null) _memos.removeAt(indexCurrentMemo);
      notifyListeners();
    } on AuthException {
      // API returned a AuthException, so user is logged out.
      await accountProvider.logOut(true);
    } catch (Exception) {
      print(Exception);
    }
  }

  // ignore: todo
  // TODO: implementation of get memo/{id}
}
