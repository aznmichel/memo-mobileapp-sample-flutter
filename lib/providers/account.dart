import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:memoplace/models/user.dart';
import 'package:memoplace/services/account_api.dart';
import 'package:memoplace/utils/exceptions.dart';
import 'package:memoplace/utils/secure_storage.dart';
import 'package:memoplace/widgets/notifications.dart';

enum Status { Uninitialized, Authenticated, Authenticating, Unauthenticated }

class AccountProvider with ChangeNotifier {
  // Variables
  Status _status = Status.Uninitialized;
  NotificationText _notification;
  User _currentUser;

  // Provides access to private variables
  Status get status => _status;
  NotificationText get notification => _notification;
  User get currentUser => _currentUser;

  // API Service
  AccountApiService accountApiService;

  // Constructor
  AccountProvider() {
    this.accountApiService = AccountApiService(client: http.Client());
  }

  void init() async {
    // initialize Singleton class for FlutterSecureStorage
    await StorageUtil.getInstance();

    String token = await StorageUtil.getValue('access_token');
    if (token != null && token.isNotEmpty) {
      _status = Status.Authenticated;
    } else {
      _status = Status.Unauthenticated;
    }
    notifyListeners();
  }

  Future<bool> login(String email, String password) async {
    _status = Status.Authenticating;
    _notification = null;
    notifyListeners();

    // API Call
    final response = await this.accountApiService.login(email, password);

    // Si enregistrement valide > Authentification ok
    if (response.statusCode == 200) {
      _status = Status.Authenticated;
      await storeToken(json.decode(response.body));
      notifyListeners();
      return true;
    }
    // Gestion des erreurs de validation > Authentification ko
    else if (response.statusCode == 401) {
      _status = Status.Unauthenticated;
      _notification = NotificationText('Invalid email or password.');
      notifyListeners();
      return false;
    }
    // Sinon erreur 500 > Authentification ko
    _status = Status.Unauthenticated;
    _notification = NotificationText('Server error.');
    notifyListeners();
    return false;
  }

  Future<bool> register(String name, String email, String password,
      String passwordConfirm) async {
    _status = Status.Authenticating;
    _notification = null;
    notifyListeners();

    // API Call
    final response = await this
        .accountApiService
        .register(name, email, password, passwordConfirm);

    // Si enregistrement valide > Authentification ok
    if (response.statusCode == 200) {
      _status = Status.Authenticated;
      await storeToken(json.decode(response.body));
      notifyListeners();
      return true;
    }
    // Gestion des erreurs de validation > Authentification ko
    else if (response.statusCode == 422) {
      _status = Status.Unauthenticated;
      Map apiResponse = json.decode(response.body);
      _notification = NotificationText(apiResponse['error'].toString());
      notifyListeners();
      return false;
    }
    // Sinon erreur 500 > Authentification ko
    _status = Status.Unauthenticated;
    _notification = NotificationText('Server error.');
    notifyListeners();
    return false;
  }

  Future<void> getUser() async {
    try {
      User currentUser = await this.accountApiService.getUserInfos();
      _currentUser = currentUser;
      notifyListeners();
    } on AuthException {
      // API returned a AuthException, so user is logged out.
      await this.logOut(true);
    } catch (Exception) {
      print(Exception);
    }
  }

  logOut([bool tokenExpired = false]) async {
    _status = Status.Unauthenticated;
    if (tokenExpired == true) {
      _notification = NotificationText('Session expired. Please log in again.',
          type: 'info');
    }
    notifyListeners();

    // API Call
    await this.accountApiService.logOut();
    // FlutterSecureStorage clear values
    StorageUtil.reset();
  }

  storeToken(apiResponse) async {
    // FlutterSecureStorage store values
    StorageUtil.putValue('access_token', apiResponse['access_token']);
    StorageUtil.putValue('refresh_token', apiResponse['refresh_token']);
  }
}
