import 'package:flutter_secure_storage/flutter_secure_storage.dart';

// Singleton class for FlutterSecureStorage
class StorageUtil {
  static StorageUtil _storageUtil;
  static FlutterSecureStorage _secureStorage;

  static Future<StorageUtil> getInstance() async {
    if (_storageUtil == null) {
      // keep local instance till it is fully initialized.
      var secureStorage = StorageUtil._();
      await secureStorage._init();
      _storageUtil = secureStorage;
    }
    return _storageUtil;
  }

  StorageUtil._();
  Future _init() async {
    _secureStorage = FlutterSecureStorage();
  }

  // Read string in FlutterSecureStorage
  static Future<String> getValue(String key, {String defValue = ''}) async {
    if (_secureStorage == null) return defValue;
    return await _secureStorage.read(key: key) ?? defValue;
  }

  // Write string in FlutterSecureStorage
  static Future<void> putValue(String key, String value) async {
    if (_secureStorage == null) return null;
    return await _secureStorage.write(key: key, value: value);
  }

  // Clean all FlutterSecureStorage
  static Future<void> reset() async {
    if (_secureStorage == null) return null;
    await _secureStorage.deleteAll();
  }
}
