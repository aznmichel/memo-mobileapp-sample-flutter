import 'package:memoplace/models/memo.dart';

// Prise en compte de la pagination
class MemoPlaceResponse {
  final List<Memo> memos;
  final String apiMore;
  MemoPlaceResponse(this.memos, this.apiMore);
}
