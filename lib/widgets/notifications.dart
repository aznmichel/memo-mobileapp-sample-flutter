import 'package:flutter/material.dart';
import 'package:memoplace/constants/Theme.dart';

class NotificationText extends StatelessWidget {
  final String text;
  final String type;

  NotificationText(this.text, {this.type, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color color = ArgonColors.error;

    if ('info' == type) {
      color = ArgonColors.info;
    }

    return Text(text,
        textAlign: TextAlign.center,
        style: TextStyle(color: color, fontSize: 11));
  }
}
