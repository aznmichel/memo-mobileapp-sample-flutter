import 'package:flutter/material.dart';
import 'package:memoplace/constants/Theme.dart';

class CardSmall extends StatelessWidget {
  CardSmall(
      {this.title = "Placeholder Title",
      this.cta = "",
      this.img = "",
      this.tap = defaultFunc,
      this.action = false,
      this.actionFunc = defaultFunc});

  final String cta;
  final String img;
  final Function tap;
  final String title;
  final bool action;
  final Function actionFunc;

  static void defaultFunc() {
    print("the function works!");
  }

  Widget get cardImage {
    var avatar = Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(6.0), topRight: Radius.circular(6.0)),
          image: img.isNotEmpty
              ? DecorationImage(
                  image: NetworkImage(img),
                  fit: BoxFit.cover,
                )
              : null),
    );

    // Placeholder is a static container the same size as the dog image.
    var placeholder = Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(6.0), topRight: Radius.circular(6.0)),
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            ArgonColors.info,
            ArgonColors.primary,
          ],
        ),
      ),
      alignment: Alignment.center,
      child: Text(
        'Loading...',
        textAlign: TextAlign.center,
        style: TextStyle(
          color: ArgonColors.text,
          fontWeight: FontWeight.w600,
          fontSize: 16,
        ),
      ),
    );

    // This is an animated widget built into flutter.
    return AnimatedCrossFade(
      firstChild: placeholder,
      secondChild: avatar,
      crossFadeState:
          img.isEmpty ? CrossFadeState.showFirst : CrossFadeState.showSecond,
      duration: Duration(milliseconds: 1000),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Container(
        height: 250,
        child: GestureDetector(
          onTap: tap,
          child: Card(
              elevation: 0.4,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0))),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    flex: 2,
                    child: cardImage, // Widget cardImage
                  ),
                  Flexible(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(title,
                                style: TextStyle(
                                    color: ArgonColors.header, fontSize: 13)),
                            Padding(
                              padding: const EdgeInsets.only(top: 4.0),
                              child: Text(
                                cta,
                                style: TextStyle(
                                    color: ArgonColors.primary,
                                    fontSize: 11,
                                    fontWeight: FontWeight.w600),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                                softWrap: false,
                              ),
                            ),
                          ],
                        ),
                      )),
                  if (action)
                    Flexible(
                      flex: 1,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ButtonBar(children: <Widget>[
                            FlatButton(
                              child: Icon(
                                Icons.remove_circle,
                                color: ArgonColors.error,
                              ),
                              onPressed: actionFunc,
                            ),
                          ]),
                        ],
                      ),
                    )
                ],
              )),
        ),
      ),
    );
  }
}
