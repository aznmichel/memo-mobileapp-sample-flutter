import 'package:flutter/material.dart';
import 'package:memoplace/constants/Theme.dart';

import 'package:memoplace/utils/validate_fields.dart';

class Input extends StatelessWidget {
  final String placeholder;
  final Widget suffixIcon;
  final Widget prefixIcon;
  final Function onTap;
  final Function onChanged;
  final TextEditingController controller;
  final TextEditingController compareController;
  final bool autofocus;
  final Color borderColor;
  final String validate;
  final bool obscureText;

  Input({
    this.placeholder,
    this.suffixIcon,
    this.prefixIcon,
    this.onTap,
    this.onChanged,
    this.autofocus = false,
    this.borderColor = ArgonColors.border,
    this.controller,
    this.validate,
    this.obscureText = false,
    this.compareController,
  });

  @override
  Widget build(BuildContext context) {
    String _validateField(String value) {
      switch (validate) {
        case "email":
          return ValidateFields.validateEmail(value);
        case "title":
          return ValidateFields.requiredField(value, 'Un titre est requis.');
        case "content":
          return ValidateFields.requiredField(value, 'Un contenu est requis.');
        case "pseudo":
          return ValidateFields.requiredField(value, 'Un pseudo est requis.');
        case "password":
          return ValidateFields.requiredField(
              value, 'Un mot de passe est requis.');
        case "confirm_password":
          if (value.isEmpty)
            return 'Un mot de passe de confirmation est requis.';
          else if (value != compareController.text)
            return 'Le mot de passe de confirmation ne matche pas.';
          return null;
        default:
          return null;
      }
    }

    return TextFormField(
        cursorColor: ArgonColors.muted,
        onTap: onTap,
        onChanged: onChanged,
        controller: controller,
        autofocus: autofocus,
        obscureText: obscureText,
        style:
            TextStyle(height: 0.85, fontSize: 14.0, color: ArgonColors.initial),
        textAlignVertical: TextAlignVertical(y: 0.6),
        decoration: InputDecoration(
            filled: true,
            fillColor: ArgonColors.white,
            hintStyle: TextStyle(
              color: ArgonColors.muted,
            ),
            suffixIcon: suffixIcon,
            prefixIcon: prefixIcon,
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(4.0),
                borderSide: BorderSide(
                    color: borderColor, width: 1.0, style: BorderStyle.solid)),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(4.0),
                borderSide: BorderSide(
                    color: borderColor, width: 1.0, style: BorderStyle.solid)),
            hintText: placeholder),
        validator: (value) {
          return _validateField(value);
        });
  }
}
