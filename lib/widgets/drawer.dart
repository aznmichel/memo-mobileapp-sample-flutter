import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:memoplace/constants/Theme.dart';

import 'package:provider/provider.dart';
import 'package:memoplace/providers/account.dart';

import 'package:memoplace/widgets/drawer-tile.dart';

class ArgonDrawer extends StatelessWidget {
  final String currentPage;

  ArgonDrawer({this.currentPage});

  _launchURL() async {
    const url = 'https://creative-tim.com';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: Container(
      color: ArgonColors.white,
      child: Column(children: [
        Container(
            height: MediaQuery.of(context).size.height * 0.1,
            width: MediaQuery.of(context).size.width * 0.85,
            child: SafeArea(
              bottom: false,
              child: Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: const EdgeInsets.only(left: 32),
                  child: Image.asset("assets/img/memoplace-logo.png"),
                ),
              ),
            )),
        Expanded(
          flex: 2,
          child: ListView(
            padding: EdgeInsets.only(top: 24, left: 16, right: 16),
            children: [
              DrawerTile(
                  icon: Icons.list_alt,
                  onTap: () {
                    if (currentPage != "Memos")
                      Navigator.pushReplacementNamed(context, '/memos');
                  },
                  iconColor: ArgonColors.warning,
                  title: "Mémos",
                  isSelected: currentPage == "Memos" ? true : false),
              DrawerTile(
                  icon: Icons.logout,
                  onTap: () {
                    // Call method logout from AccountProvider model
                    context.read<AccountProvider>().logOut();
                    // Return to the previous view
                    Navigator.of(context).pop();
                  },
                  iconColor: ArgonColors.error,
                  title: "Déconnexion",
                  isSelected: currentPage == "Déconnexion" ? true : false),
            ],
          ),
        ),
        Expanded(
          flex: 1,
          child: Container(
              padding: EdgeInsets.only(left: 8, right: 16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Divider(height: 4, thickness: 0, color: ArgonColors.muted),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 16.0, left: 16, bottom: 8),
                    child: Text("DOCUMENTATION ARGON",
                        style: TextStyle(
                          color: Color.fromRGBO(0, 0, 0, 0.5),
                          fontSize: 15,
                        )),
                  ),
                  DrawerTile(
                      icon: Icons.article,
                      onTap: _launchURL,
                      iconColor: ArgonColors.muted,
                      title: "Getting Started",
                      isSelected:
                          currentPage == "Getting started" ? true : false),
                ],
              )),
        ),
      ]),
    ));
  }
}
