import 'dart:convert';

import 'package:equatable/equatable.dart';

Memo memoFromJson(String str) => Memo.fromJson(json.decode(str));

String memoToJson(Memo data) => json.encode(data.toJson());

List<Memo> memoListFromJson(String str) =>
    new List<Memo>.from(json.decode(str).map((x) => Memo.fromJson(x)));

String memoListToJson(List<Memo> data) =>
    json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class Memo extends Equatable {
  final id;
  final String title;
  final String content;
  final status;
  final scope;
  final createdAt;
  final updatedAt;

  Memo(
      {this.id,
      this.title,
      this.content,
      this.status,
      this.scope,
      this.createdAt,
      this.updatedAt});

  factory Memo.fromJson(Map<String, dynamic> json) => Memo(
        id: json["id"],
        title: json["title"],
        content: json["content"],
        status: json["status"],
        scope: json["scope"],
        createdAt: json["createdAt"],
        updatedAt: json["updatedAt"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "content": content,
        "status": status,
        "scope": scope,
        "createdAt": createdAt,
        "updatedAt": updatedAt,
      };

  @override
  List<Object> get props =>
      [id, title, content, status, scope, createdAt, updatedAt];
}
