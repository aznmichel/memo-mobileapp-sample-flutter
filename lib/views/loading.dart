import 'package:flutter/material.dart';

// Providers
import 'package:provider/provider.dart';
import 'package:memoplace/providers/account.dart';

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // initAccountProvider
    context.watch<AccountProvider>().init();

    return Scaffold(
      appBar: AppBar(
        title: Text("Argon Flutter - Memoplace"),
      ),
      body: Center(
        child: Container(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
            child: new CircularProgressIndicator(),
          ),
        ),
      ),
    );
  }
}
