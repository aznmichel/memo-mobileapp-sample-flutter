import 'package:flutter/material.dart';
import 'package:memoplace/constants/Theme.dart';
import 'package:memoplace/widgets/input.dart';

import 'package:memoplace/models/memo.dart';

import 'package:provider/provider.dart';
import 'package:memoplace/providers/memo.dart';

import 'package:memoplace/widgets/notifications.dart';

class MemoForm extends StatefulWidget {
  final Memo memo;

  const MemoForm(this.memo, {Key key}) : super(key: key);

  @override
  MemoFormState createState() => MemoFormState();
}

class MemoFormState extends State<MemoForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController _title;
  TextEditingController _content;

  @override
  void initState() {
    super.initState();
    if (widget.memo != null) {
      _title = TextEditingController(text: widget.memo.title);
      _content = TextEditingController(text: widget.memo.content);
    } else {
      _title = TextEditingController(text: "");
      _content = TextEditingController(text: "");
    }
  }

  Future<void> submit() async {
    if (_formKey.currentState.validate()) {
      if (widget.memo != null)
        // Update an existing Memo
        await context
            .read<MemoProvider>()
            .udpateMemo(_title.text, _content.text, widget.memo.id);
      else
        // Create a new Memo
        await context.read<MemoProvider>().addMemo(_title.text, _content.text);

      // If no errors return
      if (context.read<MemoProvider>().notification == null)
        Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Container(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: new Wrap(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Text(
                      (widget.memo != null) ? "Edition Mémo" : "Nouveau Mémo",
                      style: TextStyle(color: ArgonColors.text, fontSize: 25)),
                ),
              ),
              SizedBox(height: 10.0),
              Center(
                child: context.select(
                    (MemoProvider m) => m.notification ?? NotificationText('')),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Input(
                  placeholder: "Titre",
                  prefixIcon: Icon(Icons.book),
                  controller: _title,
                  validate: "title",
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Input(
                  placeholder: "Ecrivez ce qui vous passe par la tête...",
                  prefixIcon: Icon(Icons.history_edu),
                  controller: _content,
                  validate: "content",
                ),
              ),
              ButtonBar(
                children: <Widget>[
                  new FlatButton(
                    color: ArgonColors.primary,
                    textColor: ArgonColors.white,
                    child: new Text('Enregistrer'),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4.0),
                    ),
                    onPressed: submit,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _title.dispose();
    _content.dispose();
    super.dispose();
  }
}
