import 'package:flutter/material.dart';
import 'dart:ui';

// Argon design
import 'package:memoplace/constants/Theme.dart';

// Providers
import 'package:provider/provider.dart';
import 'package:memoplace/providers/account.dart';

// Widgets
import 'package:memoplace/widgets/navbar.dart';
import 'package:memoplace/widgets/input.dart';
import 'package:memoplace/widgets/notifications.dart';

class Register extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: Navbar(
          title: "Register",
          transparent: true,
          leftOptions: false,
          rightOptions: false,
        ),
        extendBodyBehindAppBar: true,
        body: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/img/register-bg.png"),
                      fit: BoxFit.cover)),
            ),
            SafeArea(
              child: ListView(children: [
                Padding(
                  padding: const EdgeInsets.only(left: 24.0, right: 24.0),
                  child: Card(
                      elevation: 5,
                      clipBehavior: Clip.antiAlias,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(4.0),
                      ),
                      child: Column(
                        children: [
                          RegisterForm(),
                        ],
                      )),
                ),
              ]),
            )
          ],
        ));
  }
}

class RegisterForm extends StatefulWidget {
  const RegisterForm({Key key}) : super(key: key);

  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController _name;
  TextEditingController _email;
  TextEditingController _password;
  TextEditingController _confirmPassword;
  bool _checkboxValue = false;

  final double height = window.physicalSize.height;

  @override
  void initState() {
    super.initState();
    _name = TextEditingController(text: "");
    _email = TextEditingController(text: "");
    _password = TextEditingController(text: "");
    _confirmPassword = TextEditingController(text: "");
  }

  Future<void> submit() async {
    final form = _formKey.currentState;
    if (form.validate()) {
      // Call method register from AccountProvider model
      final register = await context.read<AccountProvider>().register(
          _name.text, _email.text, _password.text, _confirmPassword.text);
      // If register successfull
      if (register) Navigator.pop(context);
    }
  }

  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Container(
        height: MediaQuery.of(context).size.height * 0.80,
        color: Color.fromRGBO(244, 245, 247, 1),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Center(
                  child: Text("Enregistrez-vous pour créer votre compte",
                      style: TextStyle(color: ArgonColors.text, fontSize: 15)),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 10.0),
                    Center(
                      child: context.select((AccountProvider accountProvider) =>
                          accountProvider.notification ?? NotificationText('')),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Input(
                        placeholder: "Pseudo",
                        prefixIcon: Icon(Icons.school),
                        controller: _name,
                        validate: "pseudo",
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Input(
                        placeholder: "E-mail",
                        prefixIcon: Icon(Icons.email),
                        controller: _email,
                        validate: "email",
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Input(
                        placeholder: "Mot de passe",
                        prefixIcon: Icon(Icons.lock),
                        obscureText: true,
                        controller: _password,
                        validate: "password",
                      ),
                    ),
                    Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Input(
                          placeholder: "Confirmation de mot de passe",
                          prefixIcon: Icon(Icons.lock),
                          obscureText: true,
                          controller: _confirmPassword,
                          validate: "confirm_password",
                          compareController: _password,
                        )),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Checkbox(
                        activeColor: ArgonColors.primary,
                        onChanged: (bool newValue) =>
                            setState(() => _checkboxValue = newValue),
                        value: _checkboxValue),
                    Text("J'accepte la",
                        style:
                            TextStyle(color: ArgonColors.muted, fontSize: 12)),
                    GestureDetector(
                        onTap: () {},
                        child: Container(
                          margin: EdgeInsets.only(left: 5),
                          child: Text("Politique de confidentialité",
                              style: TextStyle(
                                  color: ArgonColors.primary, fontSize: 12)),
                        )),
                  ],
                ),
                context.watch<AccountProvider>().status == Status.Authenticating
                    ? Center(child: CircularProgressIndicator())
                    : Center(
                        child: FlatButton(
                          textColor: ArgonColors.white,
                          color: ArgonColors.primary,
                          onPressed: submit,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(4.0),
                          ),
                          child: Padding(
                              padding: EdgeInsets.only(
                                  left: 16.0, right: 16.0, top: 12, bottom: 12),
                              child: Text("CREER MON COMPTE",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16.0))),
                        ),
                      ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _name.dispose();
    _email.dispose();
    _password.dispose();
    _confirmPassword.dispose();
    super.dispose();
  }
}
