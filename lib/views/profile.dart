import 'package:flutter/material.dart';
import 'dart:ui';

// Argon design
import 'package:memoplace/constants/Theme.dart';

// Providers
import 'package:provider/provider.dart';
import 'package:memoplace/providers/account.dart';

// Widgets
import 'package:memoplace/widgets/navbar.dart';
import 'package:memoplace/widgets/drawer.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Widget photo profil
    Widget photoProfile = Hero(
      tag: "profile",
      child: FractionalTranslation(
          translation: Offset(0.0, -0.5),
          child: Align(
            child: CircleAvatar(
              backgroundImage:
                  AssetImage("assets/img/profile-screen-avatar.jpg"),
              radius: 65.0,
              // maxRadius: 200.0,
            ),
            alignment: FractionalOffset(0.5, 0.0),
          )),
    );

    // Widget name profil
    Widget identityProfile = Align(
      child: Text(context.select((AccountProvider a) => a.currentUser.name),
          style:
              TextStyle(color: Color.fromRGBO(50, 50, 93, 1), fontSize: 28.0)),
    );

    // Widget email profil
    Widget emailProfile = Align(
      child: Text(context.select((AccountProvider a) => a.currentUser.email),
          style: TextStyle(
              color: Color.fromRGBO(50, 50, 93, 1),
              fontSize: 18.0,
              fontWeight: FontWeight.w200)),
    );

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: Navbar(
        title: "Profil",
        transparent: true,
        rightOptions: false,
        leftOptions: true,
        backButton: true,
      ),
      backgroundColor: ArgonColors.bgColorScreen,
      drawer: ArgonDrawer(currentPage: "Profile"),
      body: Stack(
        children: <Widget>[
          Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      alignment: Alignment.topCenter,
                      image: AssetImage("assets/img/profile-screen-bg.png"),
                      fit: BoxFit.fitWidth))),
          SafeArea(
            child: ListView(
              children: [
                Padding(
                  padding:
                      const EdgeInsets.only(left: 16.0, right: 16.0, top: 74.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Stack(
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.1),
                                  spreadRadius: 1,
                                  blurRadius: 7,
                                  offset: Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ],
                            ),
                            child: Card(
                                semanticContainer: true,
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                elevation: .0,
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5.0))),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      top: 85.0, bottom: 20.0),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Column(
                                          children: [
                                            //////////////////////STATS////////////////////////
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                _buildScopesStatsColumn(
                                                    ArgonColors.white,
                                                    ArgonColors.initial,
                                                    Icons.public_outlined,
                                                    "4"),
                                                SizedBox(
                                                  width: 30.0,
                                                ),
                                                _buildScopesStatsColumn(
                                                    ArgonColors.initial,
                                                    ArgonColors.white,
                                                    Icons.location_disabled,
                                                    "4"),
                                              ],
                                            ),
                                            SizedBox(height: 40.0),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceAround,
                                              children: [
                                                _buildStatusStatsColumn(
                                                    "Ouverts", "22"),
                                                _buildStatusStatsColumn(
                                                    "Clôts", "10"),
                                                _buildStatusStatsColumn(
                                                    "Archivés", "89"),
                                              ],
                                            ),
                                            SizedBox(height: 30.0),
                                            //////////////////////PROFIL////////////////////////
                                            identityProfile, // Widget name profil
                                            SizedBox(height: 5.0),
                                            emailProfile, // Widget email profil
                                            SizedBox(height: 25.0),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                )),
                          ),
                          photoProfile, // Widget photo profil
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container _buildScopesStatsColumn(
      Color boxColor, Color contentColor, IconData icon, String stat) {
    return Container(
      decoration: BoxDecoration(
        color: boxColor,
        borderRadius: BorderRadius.circular(3.0),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 1,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Row(
        children: [
          Icon(
            icon,
            color: contentColor,
          ),
          Text(
            stat,
            style: TextStyle(
                color: contentColor,
                fontSize: 16.0,
                fontWeight: FontWeight.bold),
          ),
        ],
      ),
      padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
    );
  }

  Column _buildStatusStatsColumn(String label, String stat) {
    return Column(
      children: [
        Text(stat,
            style: TextStyle(
                color: Color.fromRGBO(82, 95, 127, 1),
                fontSize: 20.0,
                fontWeight: FontWeight.bold)),
        Text(label,
            style:
                TextStyle(color: Color.fromRGBO(50, 50, 93, 1), fontSize: 12.0))
      ],
    );
  }
}
