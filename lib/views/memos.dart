import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';

import 'package:memoplace/constants/Theme.dart';

import 'package:memoplace/models/memo.dart';

import 'package:provider/provider.dart';
import 'package:memoplace/providers/memo.dart';

import 'package:memoplace/utils/shimmers.dart';
import 'package:memoplace/views/memo_form.dart';
import 'package:memoplace/widgets/card-small.dart';
import 'package:memoplace/widgets/drawer.dart';
import 'package:memoplace/widgets/navbar.dart';

final List<Map<String, String>> homeCards = [
  {
    "title": "Ice cream is made with carrageenan …",
    "image":
        "https://images.unsplash.com/photo-1516559828984-fb3b99548b21?ixlib=rb-1.2.1&auto=format&fit=crop&w=2100&q=80"
  },
  {
    "title": "Is makeup one of your daily esse …",
    "image":
        "https://images.unsplash.com/photo-1519368358672-25b03afee3bf?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2004&q=80"
  },
  {
    "title": "Coffee is more than just a drink: It’s …",
    "image":
        "https://images.unsplash.com/photo-1500522144261-ea64433bbe27?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2102&q=80"
  },
  {
    "title": "Fashion is a popular style, especially in …",
    "image":
        "https://images.unsplash.com/photo-1487222477894-8943e31ef7b2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1326&q=80"
  },
  {
    "title": "Argon is a great free UI packag …",
    "image":
        "https://images.unsplash.com/photo-1482686115713-0fbcaced6e28?fit=crop&w=1947&q=80"
  }
];

class Memos extends StatefulWidget {
  @override
  MemosState createState() => MemosState();
}

class MemosState extends State<Memos> {
  void showMemoForm(context, [Memo memo]) {
    // Because modals do not have access to the Provider...
    final memoProvider = Provider.of<MemoProvider>(context, listen: false);

    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        // Provide an existing provider...
        return ChangeNotifierProvider.value(
          value: memoProvider,
          child: new MemoForm(memo),
        );
      },
    );
  }

  Future<void> removeMemo(int idMemo) async {
    if (idMemo >= 0)
      // Remove an existing Memo
      await context.read<MemoProvider>().removeMemo(idMemo);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    final memoProvider = context.watch<MemoProvider>();

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: Navbar(
        title: "Mémos",
        transparent: true,
        rightOptions: true,
      ),
      backgroundColor: ArgonColors.bgColorScreen,
      drawer: ArgonDrawer(currentPage: "Memos"),
      body: Stack(
        children: <Widget>[
          Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      alignment: Alignment.topCenter,
                      image: AssetImage("assets/img/onboard-background.png"),
                      fit: BoxFit.fitWidth))),
          Container(
            child: memoProvider.initialized != true
                ? ShimmerGrid()
                : memoList(context, memoProvider.memos),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // Modal Bottomsheet Memo Form (NEW)
          showMemoForm(context);
        },
        child: Icon(
          Icons.playlist_add_rounded,
          size: 30.0,
        ),
        backgroundColor: ArgonColors.primary,
      ),
    );
  }

  // Widget list Memos
  Widget memoList(BuildContext context, List<Memo> memos) {
    ScrollController _scrollController = ScrollController();

    /*_scrollController.addListener(() {
      if (_scrollController.position.maxScrollExtent ==
          _scrollController.position.pixels) {
        loadMore();
      }
    });*/

    return memos.length == 0
        ? Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text("Aucun mémos...",
                      style: TextStyle(
                          color: ArgonColors.white,
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold)),
                ]),
          )
        : GridView.builder(
            controller: _scrollController,
            itemCount: memos.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              childAspectRatio: 8.0 / 10.0,
              crossAxisCount: 2,
            ),
            itemBuilder: (BuildContext context, int index) {
              final memo = memos[index];
              int randomNumber = Random().nextInt(5);
              String urlImage = homeCards[randomNumber]['image'];

              return Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CardSmall(
                      img: urlImage,
                      title: memo.title.toUpperCase(),
                      cta: memo.content,
                      tap: () {
                        // Modal Bottomsheet Memo Form (EDITION)
                        showMemoForm(context, memo);
                      },
                      action: true,
                      actionFunc: () {
                        // Display alert remove Memo (confirmation)
                        showAlertDialog(context, memo.id);
                      },
                    )
                  ]);
            },
          );
  }

  // Alert remove Memo (confirmation)
  showAlertDialog(BuildContext context, int memoId) {
    // Set up the buttons
    Widget cancelButton = FlatButton(
      color: ArgonColors.secondary,
      textColor: ArgonColors.text,
      child: new Text('Annuler'),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4.0),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget launchButton = FlatButton(
        color: ArgonColors.error,
        textColor: ArgonColors.white,
        child: new Text('Confirmer'),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(4.0),
        ),
        onPressed: () {
          removeMemo(memoId);
        });

    // Set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Suppression",
          style: TextStyle(color: ArgonColors.text, fontSize: 25)),
      content: Text("Etes-vous sûr de vouloir supprimer ce mémo?"),
      actions: [
        cancelButton,
        launchButton,
      ],
    );

    // Show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
