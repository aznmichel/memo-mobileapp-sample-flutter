import 'package:flutter/material.dart';
import 'dart:ui';

// Argon design
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:memoplace/constants/Theme.dart';

// Providers
import 'package:provider/provider.dart';
import 'package:memoplace/providers/account.dart';

// Widgets
import 'package:memoplace/widgets/navbar.dart';
import 'package:memoplace/widgets/input.dart';
import 'package:memoplace/widgets/notifications.dart';

class Login extends StatelessWidget {
  final _scaffoldLoginKey = GlobalKey<ScaffoldState>();

  // Display SnackBar with a text
  void showPopWarning(BuildContext context) {
    _scaffoldLoginKey.currentState.showSnackBar(
      SnackBar(
        backgroundColor: ArgonColors.primary,
        content: Text("En cours de construction..."),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // Section Login via social media (Github, Facebook, etc.)
    Widget loginSocialSection = Container(
      height: MediaQuery.of(context).size.height * 0.15,
      decoration: BoxDecoration(
          color: ArgonColors.white,
          border:
              Border(bottom: BorderSide(width: 0.5, color: ArgonColors.muted))),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Center(
            child: Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Text("Connectez-vous via les réseaux sociaux",
                  style: TextStyle(color: ArgonColors.text, fontSize: 15.0)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  // width: 0,
                  height: 36,
                  child: RaisedButton(
                      textColor: ArgonColors.primary,
                      color: ArgonColors.secondary,
                      onPressed: () {
                        showPopWarning(context);
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4)),
                      child: Padding(
                          padding: EdgeInsets.only(
                              bottom: 10, top: 10, left: 14, right: 14),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Icon(FontAwesomeIcons.github, size: 13),
                              SizedBox(width: 5),
                              Text("GITHUB",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 13))
                            ],
                          ))),
                ),
                Container(
                  // width: 0,
                  height: 36,
                  child: RaisedButton(
                      textColor: ArgonColors.primary,
                      color: ArgonColors.secondary,
                      onPressed: () {
                        showPopWarning(context);
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4)),
                      child: Padding(
                          padding: EdgeInsets.only(
                              bottom: 10, top: 10, left: 8, right: 8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Icon(FontAwesomeIcons.google, size: 13),
                              SizedBox(width: 5),
                              Text("GOOGLE",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 13))
                            ],
                          ))),
                ),
              ],
            ),
          ),
        ],
      ),
    );

    return Scaffold(
      key: _scaffoldLoginKey,
      appBar: Navbar(
        title: "Login",
        transparent: true,
        leftOptions: false,
        rightOptions: false,
      ),
      extendBodyBehindAppBar: true,
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/img/register-bg.png"),
                    fit: BoxFit.cover)),
          ),
          SafeArea(
            child: ListView(
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                      top: 2, left: 24.0, right: 24.0, bottom: 32),
                  child: Card(
                    elevation: 5,
                    clipBehavior: Clip.antiAlias,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4.0),
                    ),
                    child: Column(
                      children: [
                        loginSocialSection, // Login via Social
                        LoginForm(), // Login via Form
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class LoginForm extends StatefulWidget {
  const LoginForm({Key key}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String message = '';
  TextEditingController _email;
  TextEditingController _password;
  bool _checkboxValue = false;

  final double height = window.physicalSize.height;

  @override
  void initState() {
    super.initState();
    _email = TextEditingController(text: "");
    _password = TextEditingController(text: "");
  }

  Future<void> submit() async {
    if (_formKey.currentState.validate()) {
      // Call method login from AccountProvider model
      final login = await context
          .read<AccountProvider>()
          .login(_email.text, _password.text);
      // If login successfull
      if (login) Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    // Section LoginForm
    return Form(
      key: _formKey,
      child: Container(
        height: MediaQuery.of(context).size.height * 0.55,
        color: Color.fromRGBO(244, 245, 247, 1),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Center(
                      child: Text("Ou connectez-vous avec vos identifiants",
                          style:
                              TextStyle(color: ArgonColors.text, fontSize: 15)),
                    ),
                    SizedBox(height: 10.0),
                    Center(
                      child: context.select((AccountProvider accountProvider) =>
                          accountProvider.notification ?? NotificationText('')),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Input(
                        placeholder: "Email",
                        prefixIcon: Icon(Icons.email),
                        controller: _email,
                        validate: "email",
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Input(
                        placeholder: "Password",
                        prefixIcon: Icon(Icons.lock),
                        obscureText: true,
                        controller: _password,
                        validate: "password",
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 2.0, top: 0, bottom: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Checkbox(
                              activeColor: ArgonColors.primary,
                              onChanged: (bool newValue) =>
                                  setState(() => _checkboxValue = newValue),
                              value: _checkboxValue),
                          Text("Se souvenir de moi",
                              style: TextStyle(
                                  color: ArgonColors.muted,
                                  fontWeight: FontWeight.w200)),
                        ],
                      ),
                    ),
                  ],
                ),
                context.watch<AccountProvider>().status == Status.Authenticating
                    ? Center(child: CircularProgressIndicator())
                    : Center(
                        child: FlatButton(
                          textColor: ArgonColors.white,
                          color: ArgonColors.primary,
                          onPressed: submit,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(4.0),
                          ),
                          child: Padding(
                              padding: EdgeInsets.only(
                                  left: 16.0, right: 16.0, top: 12, bottom: 12),
                              child: Text("CONNEXION",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16.0))),
                        ),
                      ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _email.dispose();
    _password.dispose();
    super.dispose();
  }
}
