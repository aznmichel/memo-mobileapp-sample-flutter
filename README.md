# Memo MobileApp (sample) - Flutter

This is a sample application that make Memos build in Flutter. 

The app uses an external REST API (built with Laravel 8 with tools such as Laravel Fortify and Laravel Passeport) that can be accessed at [Memoplace](https://memoplace-staging.herokuapp.com/). The source code for the API is available [here](https://gitlab.com/aznmichel/memo-api-sample-laravel).

The template used comes from the [Creative Tim - Argon Flutter theme](https://demos.creative-tim.com/argon-flutter/)

## Screenshots

|                                                Onboarding Screen                                                 |                                                 Register Screen                                                  |                                                Login Screen                                                 |                                                  Home Screen                                                  |                                                  Profile Screen                                                  |
| :---------------------------------------------------------------------------------------------------------: | :--------------------------------------------------------------------------------------------------------------: | :----------------------------------------------------------------------------------------------------------: | :--------------------------------------------------------------------------------------------------------------: | :--------------------------------------------------------------------------------------------------------------: |
| ![Onboarding Screen](docs/onboarding_screen.jpg?raw=true "Onboarding_screen") | ![Register Screen](docs/register_screen.jpg?raw=true "Register") | ![Login Screen](docs/login_screen.jpg?raw=true "Login") | ![Home Screen](docs/home_screen.jpg?raw=true "Home") | ![Profile Screen](docs/profile_screen.jpg?raw=true "Profile") |

## Getting Started

### Prerequisites

- To run this project, you must have [Dart](https://dart.dev) Programming Language and [Flutter](https://flutter.dev) SDK installed in your machine.
- You must also set up the backend server for [Memoplace API](https://gitlab.com/aznmichel/memo-api-sample-laravel). You can find the instructions [here](https://gitlab.com/aznmichel/memo-api-sample-laravel).

### Step 1

After cloning the repository to your machine:
- To install dependencies, run this command in your terminal.

```bash
flutter pub get
```

### Step 2

- Next, you have to set your endpointAPI url in `lib/constants/api.dart` file as below.
```properties
static const endpointAPI = 'YOUR_LOCALHOST_URL_OR_SHARED_SITES_URL/api';
```

## Features

The app handles features like:

* Login
* Login with Social Media (in progress)
* Registration
* Add, Edit, Remove Memos
* View Home and Profile
* Log out

## Flutter Packages

Some useful packages that I used:

* `http` a robust HTTP library.
* `provider` to manage app state.
* `flutter_secure_storage` to store data in secure storage (Instead of store in `shared_preferences`).
* `shimmer` to display shimmer effect during loading data.
* `logger` to easily print beautiful logs.
* `equatable` to override == and hashCode operator for you.
* `flutter_test` and `mockito` for testing with mock usage.
* `test_coverage` to compute coverage.

## Information

This Memo app is build for Mobile (Flutter) and Desktop (VueJS) usage. The source code for the Desktop app in VueJS is available [here](https://gitlab.com/aznmichel/memo-frontend-sample-vuejs). 

![Memoplace Presentation](docs/memoplace_presentation.png?raw=true "memoplace_presentation")